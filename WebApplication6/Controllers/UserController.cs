﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class UserController : Controller
    {
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            if (user.Age < 16)
            {
                return RedirectToAction("TooYoungError");
            }

            string jsonUser = JsonSerializer.Serialize(user);
            TempData["User"] = jsonUser;

            return RedirectToAction("Quantity","Product");
        }

        public ActionResult TooYoungError()
        {
            return View();
        }
    }
}
