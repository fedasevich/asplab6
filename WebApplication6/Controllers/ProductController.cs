﻿using Microsoft.AspNetCore.Mvc;
using WebApplication6.Models;
using System.Text.Json;
using System;


namespace WebApplication6.Controllers
{
    public class ProductController : Controller
    {

        public IActionResult Quantity()
        {
          
            return View();
        }

  
        [HttpPost]
        public IActionResult Quantity(int quantity)
        {
          Console.WriteLine(quantity.ToString());

            if (quantity < 1)
            {
                return BadRequest("Quantity should be a non-negative integer.");
            }

            TempData["Quantity"] = quantity;
            return RedirectToAction("CheckOutForm");
        }

        public IActionResult CheckOutForm()
        {
            int? quantity = TempData["Quantity"] as int?;

            if (!quantity.HasValue)
            {
                return BadRequest("Quantity should be a non-negative integer.");
            }

            ViewData["Quantity"] = quantity.Value;
            return View();
        }

        [HttpPost]
        public IActionResult CheckOutForm(List<Product> products)
        {
            string json = JsonSerializer.Serialize(products);
               TempData["OrderData"]= json;

            return RedirectToAction("Order");
        }

        public IActionResult Order()
        {
            string? orderData = TempData["OrderData"] as string;
            string? userData = TempData["User"] as string;
            if (orderData == null || userData==null)
            {
                return BadRequest("Order or user data was not found.");
            }

            List<Product> products = JsonSerializer.Deserialize<List<Product>>(orderData);
            User user = JsonSerializer.Deserialize<User>(userData);
            Console.WriteLine(user.Name);
            ViewData["OrderData"] = products;
            ViewData["User"] = user;
            return View();
        }
    }
}
